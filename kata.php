<?php

function spinWords($str) {

  $str = explode(' ', $str);
  foreach ($str as $key => $word) {
    if (strlen($word) >= 5) {
      $reversed = strrev($word);
      $str[$key] = $reversed;
    }
  }

  return implode(' ', $str);
}


echo spinWords('Hey fellow warriors');
// Other string examples:
// You are almost to the last test
// Seriously this is the last one
// Just kidding there is still one more